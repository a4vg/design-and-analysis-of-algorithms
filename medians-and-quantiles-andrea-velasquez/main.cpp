#include <iostream>
#include <fmt/core.h>

#include <vector>
#include <string>
#include <cstdlib> // random
#include <ctime>
#include <cmath>
#include <algorithm> // sort

int quickselect(std::vector<int> &values, int k);
int smart_selection(std::vector<int> &values);
double median(std::vector<int> &values);
std::vector<double> quantiles(std::vector<int> &values, const std::vector<double>& quantiles);


// Auxiliary function
double log_median(std::vector<int> values){
    std::sort(values.begin(), values.end());
    return values[floor(values.size()/2)];
}

// Core median of medians
int quickselect(std::vector<int> &values, int k){
    if (values.empty()) throw std::string("Empty list");
    else if (values.size() == 1) return values[0];

    int pivot = smart_selection(values);

    std::vector<int> smallest, largest, pivots;

    for (const auto &x: values){
        if (x < pivot) smallest.push_back(x);
        else if (x > pivot) largest.push_back(x);
        else pivots.push_back(x); // in case there are repeated numbers
    }

    if (k < smallest.size()) return quickselect(smallest, k);
    else if  (k < smallest.size() + pivots.size()) return pivots[0];
    else return quickselect(largest, k - smallest.size() - pivots.size());
}

int smart_selection(std::vector<int> &values){
    if (values.size()<5) return log_median(values);

    std::vector<int> medians_of_partitions;
    int c = 0;
    for (int i=0; i<floor(values.size()/5); ++i){
        std::vector<int> temp;
        for (int j=0; j<5; ++j){
            temp.push_back(values[c]);
            ++c;
        }
        std::sort(temp.begin(), temp.end());
        medians_of_partitions.push_back(temp[2]);
    }

    return median(medians_of_partitions);
}

// Call functions...
double median(std::vector<int> &values){
    if (values.size() % 2 == 1) return (double) quickselect(values, values.size()/2);
    return ((double) quickselect(values, floor(values.size()/2)) + quickselect(values, ceil(values.size()/2)))/2;
};

std::vector<double> quantiles(std::vector<int> &values, const std::vector<double>& quantiles){
    std::vector<double> answer;
    for (const auto & quantil: quantiles){
        double partial_answer;
        if (values.size() % 2 == 1) partial_answer = (double) quickselect(values, values.size()/2);
        else partial_answer = ( (double) quickselect(values, floor( values.size() * quantil)) + (double) quickselect(values, ceil( values.size() * quantil)))/2;
        answer.push_back(partial_answer);
    }
    return answer;
};

int main() {
    srand(time(NULL));

    std::vector<int> values = {43, 38,33, 28, 23, 18, 13, 8, 49, 
                               44, 39, 34, 29, 24, 19, 14, 9, 50,
                               45, 40, 35, 30, 25, 20, 15, 10, 51,
                               46, 41, 36, 31, 26, 21, 16, 53, 52,
                               47, 42, 37, 32, 27, 22, 17, 54};
    std::cout << "Median: " << median (values) << "\n";
    std::cout << "Quantil : \n";
    for (const auto &ans : quantiles(values, {0.25, 0.75})){
         std::cout << ans << "\n";
    }
}
