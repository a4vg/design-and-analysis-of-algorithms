#include <iostream>
#include <vector>
#include <utility> // pair
#include <algorithm> // sort
#include <cmath> // round

float maxLootValue(std::vector< std::pair<int, int> > &compounds, int W) // price weight
{
    /* Description
        Find the maximal value of items that fit into the backpack.
       Approach
        Pick always the most valuable compound. If the compound
        doesn't fit in the backpack (its weight is greater than
        the backpack capacity), pick just a fraction of it.
    */

    std::sort(compounds.begin(), compounds.end(),
        [](const auto& left, const auto& right) {
            return (float) left.first/left.second > (float) right.first/right.second;
        });

    float value = 0;
    for (int i=0; i<compounds.size() && W>0; ++i)
    {
        if (compounds[i].second<W)
            value += compounds[i].first;
        else{
            value += (float)(compounds[i].first)/(float)(compounds[i].second)*W;
        }

        W -= compounds[i].second;
    }

    return round(value*10000)/10000;
}


int main()
{
    int n, W;
    std::cin >> n >> W;

    std::vector< std::pair<int, int> > compounds;
    for (int i=0; i<n; ++i)
    {
        int value, weight;
        std::cin >> value >> weight;
        compounds.push_back(std::make_pair(value, weight));
    }

    std::cout << maxLootValue(compounds, W);


    return 0;
}