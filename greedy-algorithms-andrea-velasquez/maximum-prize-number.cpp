#include <iostream>
#include <vector>

std::vector<int> prizeNumber(int n)
{
    /* Description
        Represent a positive integer as the sum of the
        maximum number of pairwise distinct positive
        integers.
       Approach
        Pick all consecutive numbers k starting from 1,
        k*2 < remaining n. This ensures there exists
        two numbers greater than k whose sum is <= remaining n;
    */

    std::vector<int> prizes;

    for (int i=1; i*2<n; ++i)
    {
        prizes.push_back(i);
        n -= i;
    }

    prizes.push_back(n);

    return prizes;
}

int main()
{
    int n;
    std::cin >> n;

    std::vector<int> prizes = prizeNumber(n);
    std::cout << prizes.size() << "\n";
    for (int p : prizes)
    {
        std::cout << p << " ";
    }
}