#include <iostream>

int moneyChange(int money)
{
    /* Description:
        Compute the minimum number of coins needed
        to change the given value into coins with
        denominations 1, 5, and 10.

       Approach:
        Take always the largest possible denomination that doesn't
        exceed the remaining amount
    */
    return (int)(money/10) + (int)((money%10)/5) + money%5;
}

int main(){
    int n;
    std::cin >> n;
    std::cout << moneyChange(n);

    return 0;
}