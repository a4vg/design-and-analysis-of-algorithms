#include <iostream>
#include <vector>
#include <string> // to_string
#include <algorithm> // sort

std::string maxSalary(std::vector<int> &numbers)
{
    /* Description
        Compile the largest number by concatenating
        the given numbers.
       Approach
        Concatenate numbers in descending alphabetical
        order. If the number digits are included in another
        (ex.: 23 is included in 234), the remaining digits of
        of the larger number are compared with the last digit
        of the smaller number. Take into account that the 
        maximum amount of digits of an input number is  
        4 (max number is 1000).
    */
    std::string salary = "";

    std::sort(numbers.begin(), numbers.end(),
            [](const int left, const int right)
            {
                std::string sleft = std::to_string(left);
                std::string sright = std::to_string(right);
                sleft.resize(4, sleft.back());
                sright.resize(4, sright.back());
                return sleft > sright;
            });

    for (const int number : numbers)
        salary += std::to_string(number);

    return salary;
}

int main()
{
    int n;
    std::cin >> n;

    std::vector<int> numbers;
    for (int i=0; i<n; ++i)
    {
        int a;
        std::cin >> a;
        numbers.push_back(a);
    }

    std::cout << maxSalary(numbers);
}