#include <iostream>
#include <vector>
#include <algorithm> // sort


int maxAdRevenue(std::vector<int> &prices, std::vector<int> &clicks)
{
    /* Description
        Find the maximum dot product of two sequences
        of numbers.
       Approach
        Multiply the largest numbers of both sequences
        together
    */

    std::sort(prices.begin(), prices.end());
    std::sort(clicks.begin(), clicks.end());

    int revenue = 0;
    for (int i=prices.size(); i>=0; --i)
        revenue += prices[i]*clicks[i];

    return revenue;
}


int main()
{
    std::vector<int> prices, clicks;

    int n;
    std::cin >> n;

    for (int i=0; i<n; ++i)
    {
        int p;
        std::cin >> p;
        prices.push_back(p);
    }

    for (int i=0; i<n; ++i)
    {
        int c;
        std::cin >> c;
        clicks.push_back(c);
    }

    std::cout << maxAdRevenue(prices, clicks);
}