#include <iostream>
#include <vector>
#include <utility> // pair, make_pair


std::vector<int> collectSignatures(std::vector< std::pair<int, int> > &segments)
{
    /* Description
        Find the minimum number of points needed
        to cover all given segments on a line.
       Approach
        Pick the lowest right points of the segments
        that are not covered yet.
    */

    std::sort(segments.begin(), segments.end(),
            [](const auto& left, const auto& right)
            {
                return left.second < right.second;
            });

    std::vector<int> points = {segments[0].second};

    for (const auto& segment : segments)
        if (segment.first>points.back() || points.back()>segment.second)
            points.push_back(segment.second);

    return points;
}


int main()
{
    int n;
    std::cin >> n;

    std::vector< std::pair<int, int> > segments;
    for (int i=0; i<n; ++i)
    {
        int p1, p2;
        std::cin >> p1 >> p2;
        segments.push_back(std::make_pair(p1, p2));
    }

    std::vector<int> points = collectSignatures(segments);

    std::cout << points.size() << "\n";

    for (const int point : points)
    {
        std::cout << point << " ";
    }

}