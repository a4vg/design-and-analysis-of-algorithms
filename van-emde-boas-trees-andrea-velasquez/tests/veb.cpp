#include <random>
#include <algorithm> // shuffle
#include <iostream>

#include "gtest/gtest.h"

#include "veb.h"

int get_rand(int a, int b) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<> dist(a, b);
    return dist(mt);
}


struct VebInsertTest : public ::testing::Test
{
};

TEST_F(VebInsertTest, Empty)
{
    int tree_size = 16;
    Veb tree(tree_size); 
    int number = get_rand(0, tree_size-1);
    tree.insert(number);

    bool response = tree.find(number);
    bool expected = true;

    EXPECT_EQ(response, expected);
}

TEST_F(VebInsertTest, Average)
{
    int tree_size = 32;
    Veb tree(tree_size);
    std::vector<int> numbers;

    for (int i=0; i<20; ++i){
        int n = get_rand(0, tree_size-1);
        numbers.push_back(n);
        tree.insert(n);
    }

    bool response = true;
    for (int i=0; i<20; ++i){
        response = response && tree.find(numbers[i]);
    }
    bool expected = true;

    EXPECT_EQ(response, expected);
}

TEST_F(VebInsertTest, Full)
{
    int tree_size = 64;
    Veb tree(tree_size);
    std::vector<int> numbers;

    for (int i=0; i<tree_size; ++i) numbers.push_back(i);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::shuffle(numbers.begin(), numbers.end(), mt);

    for (int i=0; i<tree_size; ++i) tree.insert(numbers[i]);
    for (int i=0; i<30; ++i) tree.insert(get_rand(0, tree_size));

    bool response = true;
    for (int i=0; i<20; ++i){
        response = response && tree.find(numbers[i]);
    }
    bool expected = true;
    
    EXPECT_EQ(response, expected);
}


TEST_F(VebSuccessorTest, Empty)
{
    int tree_size = 32;
    Veb tree(tree_size);

    int n = get_rand(0, numbers.size()-1);
    int response = tree.successor(n);
    int expected = -1;
    EXPECT_EQ(response, expected);
}

TEST_F(VebSuccessorTest, Average)
{
    int tree_size = 32;
    Veb tree(tree_size);
    std::vector<int> numbers;

    for (int i=0; i<20; ++i){
        int n = get_rand(0, tree_size-1);
        numbers.push_back(n);
        tree.insert(n);
    }

    std::sort(numbers.begin(), numbers.end());
    numbers.push_back(-1);

    for (int i=0; i<10; ++i){
        int rand_index = get_rand(0, numbers.size()-2); // avoid -1
        int response = tree.successor(numbers[rand_index]);
        int expected = numbers[rand_index+1];
        EXPECT_EQ(response, expected);
    }
}

TEST_F(VebSuccessorTest, Full)
{
    int tree_size = 32;
    Veb tree(tree_size);
    std::vector<int> numbers;

    for (int i=0; i<tree_size; ++i) numbers.push_back(i);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::shuffle(numbers.begin(), numbers.end(), mt);
    for (int i=0; i<tree_size; ++i) tree.insert(numbers[i]);

    for (int i=0; i<tree_size; ++i){
        int response = tree.successor(i);
        int expected = i+1;
        if (expected==tree_size) expected = -1;

        EXPECT_EQ(response, expected);
    }
}



TEST_F(VebFindTest, Empty)
{
    int tree_size = 32;
    Veb tree(tree_size);

    int n = get_rand(0, tree_size-1);
    bool response = tree.find(n);
    bool expected = false;
    EXPECT_EQ(response, expected);
}

TEST_F(VebFindTest, Average)
{
    int tree_size = 32;
    Veb tree(tree_size);
    std::vector<int> numbers;

    for (int i=0; i<20; ++i){
        int n = get_rand(0, tree_size-1);
        numbers.push_back(n);
        tree.insert(n);
    }

    for (int i=0; i<numbers.size(); ++i){
        int n = get_rand(0, numbers.size()-1);
        bool response = tree.find(n);
        bool expected = std::find(numbers, numbers+numbers_size, rand_num)!=(numbers+numbers_size);
        EXPECT_EQ(response, expected);
    }
}


TEST_F(VebFindTest, Full)
{
    int tree_size = 32;
    Veb tree(tree_size);

    for (int i=0; i<tree_size; ++i){
        tree.insert(n);
    }

    for (int i=0; i<tree_size; ++i){
        bool response = tree.find(n);
        bool expected = true;
        EXPECT_EQ(response, expected);
    }
}
