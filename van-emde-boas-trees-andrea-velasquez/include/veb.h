#include <vector>
#include <cmath>
#include <cassert>

#ifndef TREE_VEB_H
#define TREE_VEB_H


class Veb {
    int u = 2;
    int min = -1;
    int max = -1;
    Veb* summary = nullptr;
    std::vector<Veb> clusters;

        int high(int x);
        int low(int x);
        int index(int x, int y);

    public:
        void insert(int x);
        int successor(int x);
        bool find(int x);

        Veb(int u);
};


#endif //TREE_VEB_H
