#include "veb.h"

#define isPower2(x) (x && (x & (x - 1)) == 0)
#define lowerSqrt(x) ((int) pow(2, (int) log2(x)/2))
void swap(int &x, int &y) { int t=x; x=y; y=t; }


int Veb::high(int x){ return x/lowerSqrt(u); }
int Veb::low(int x){ return x % lowerSqrt(u); }
int Veb::index(int x, int y){ return x*lowerSqrt(u) + y; }

void Veb::insert(int x){
    assert(0<=x && x<u);

    // Empty
    if (min<0) { min = max = x; return; }

    // Non-empty
    if (x<min){
        swap(x, min);
    }

    if (x>max) {max = x;}

    if (u>2){ // not base case
        if (clusters[high(x)].min<0){ // cluster was empty --> summary needs to be updated
            summary->insert(high(x));
        }
        clusters[high(x)].insert(low(x)); // if cluster was empty, this is O(1)
    }

}

int Veb::successor(int x){
    assert(0<=x && x<u);

    if (u==2){ // base case
        if (x==0 && max==1) return 1;
        return -1; // x is the max or cluster is empty --> successor doesn't exist
    }
    // For u>2:
    if (min!=-1 && x<min) return min; // local cluster is not empty and since min>x (x is unset), the successor is min

    // check if we're going to find a successor in the local cluster
    // to avoid making a pointless recursion
    int cluster_max = clusters[high(x)].max;
    int cluster, offset;
    if (cluster_max!=-1 && low(x)<cluster_max){ // x is left to max
        cluster = high(x);
        offset = clusters[high(x)].successor(low(x));
    }
    else{ // x is right to the max --> look the nearest cluster with a min
        cluster = summary->successor(high(x));
        if (cluster==-1) return -1; // all next clusters are empty
        offset = clusters[cluster].min;
    }
    return index(cluster, offset);
}

bool Veb::find(int x){
    if (x==min || x==max) return true;
    if (u==2) return false;
    return clusters[high(x)].find(low(x));
}

Veb::Veb(int _u){
    if (!isPower2(_u) || _u==2) return;

    u = _u;
    int clust_size = sqrt(u);
    for (int i=0; i<clust_size; ++i) { clusters.emplace_back(clust_size); }
    summary = new Veb(clust_size);
}