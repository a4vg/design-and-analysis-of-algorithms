#include "graph.h"

#include <cassert>
#include <iostream>

extern const int INF;

Graph::Graph(int _n){
    n = _n;
    std::vector<int> aux(n, INF);
    for (int i=0; i<n; ++i) AM.push_back(aux);
}

int Graph::size(){
    return n;
}

bool Graph::insert_edge(int _s, int _t, int _w){
    assert(0<=_s<n && 0<=_t<n);
    if (AM[_s][_t]!=INF) return false;
    AM[_s][_t] = _w;
    return true;
}

std::vector<std::pair<int, int>> Graph::edges_of(int _v){
    std::vector<std::pair<int, int>> edges;
    for (int i=0; i<n; ++i){
        if (AM[_v][i]!=INF) edges.push_back(std::make_pair(i, AM[_v][i]));
    }
    return edges;
}

void Graph::print(){
    // header
    std::cout <<"  ";
    for (int i=0; i<n; ++i){
        std::cout << i << " ";
    }
    std::cout << "\n";
    // content
    for (int i=0; i<n; ++i){
        std::cout << i << " ";
        for (int j=0; j<n; ++j){
            if (AM[i][j] == INF) std::cout << "-" << " ";
            else std::cout << AM[i][j] << " ";
        }
        std::cout << "\n";
    }
}
