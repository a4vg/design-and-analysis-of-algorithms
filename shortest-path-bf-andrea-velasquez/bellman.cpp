#include <iostream>
#include "graph.h"

extern const int INF;

std::vector<int> bellmanFord(Graph G, int begin){
    assert(0<=begin<G.size());

    std::vector<int> dist(G.size(), INF);
    dist[begin] = 0;
    for (int i=0; i<G.size(); ++i){
        for (int source=0; source<G.size(); ++source){
            for (const auto& edge : G.edges_of(source)){
                int target = edge.first;
                int weight = edge.second;
                if (dist[source]+weight < dist[target]){
                    dist[target] = dist[source]+weight;
                }
            }
        }
    }

    // Check for negative cycles
    for (int source=0; source<G.size(); ++source){
        for (const auto& edge : G.edges_of(source)){
            int target = edge.first;
            int weight = edge.second;
            if (dist[source] + weight < dist[target])
                throw "Negative cycle found in graph";
        }
    }
    return dist;
}

int main(){
   Graph graph(6);
   graph.insert_edge(0, 1, 10);
   graph.insert_edge(0, 5, 8);
   graph.insert_edge(1, 3, 2);
   graph.insert_edge(2, 1, 1);
   graph.insert_edge(3, 2, -2);
   graph.insert_edge(4, 1, -4);
   graph.insert_edge(4, 3, -1);
   graph.insert_edge(5, 4, 1);

   std::cout << "Adjacent matrix of graph:\n";
   graph.print();

   int begin = 0;
   std::vector<int> distances = bellmanFord(graph, begin);
   for (int i=0; i<distances.size(); ++i)
       std::cout << "\nMinimum distance from " << begin << " to " << i << " is " << distances[i];
}