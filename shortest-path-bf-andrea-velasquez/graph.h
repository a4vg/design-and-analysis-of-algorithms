#include <vector>
#include <utility>

const int INF = 999999999;

class Graph{
    std::vector<std::vector<int>> AM;
    int n;

    public:
        Graph(int n);
        int size();
        bool insert_edge(int _s, int _t, int _w);
        std::vector<std::pair<int, int>> edges_of(int _v); // pair = (target, weight)
        void print();
};